# ROT13 CLI

A simple CLI tool that "encrypts" text using rot13

## Usage

```shell
rot13 [input]
echo [input] | rot13
```

## Installing

```shell
git clone git@gitlab.com:dbgit/open/rot13-cli.git
cd rot13-cli
cargo install --path .
```