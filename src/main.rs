use std::env;
use std::io;
use std::io::BufRead;
use std::process;

fn main() {
    // check for direct params
    let args: Vec<String> = env::args().collect();
    let arg_input = args.get(1);

    if let Some(x) = arg_input {
        println!("{}", rot13(x));
        process::exit(0);
    }

    // fallback to stdin if no params have been supplied directly
    let mut std_input = String::new();
    let stdin = io::stdin();
    let mut handle = stdin.lock();
    handle.read_line(&mut std_input).unwrap();

    if !std_input.is_empty() {
        println!("{}", rot13(&std_input));
        process::exit(0);
    }

    // exit with error if neither direct params nor stdin have been supplied
    process::exit(1);
}

pub fn rot13(text: &str) -> String {
    text.chars().map(|c| {
        match c {
            'A' ..= 'M' | 'a' ..= 'm' => ((c as u8) + 13) as char,
            'N' ..= 'Z' | 'n' ..= 'z' => ((c as u8) - 13) as char,
            _ => c
        }
    }).collect()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn rot13_test() {
        assert_eq!(rot13("abc123"), "nop123");
        assert_eq!(rot13("KlmnoP"), "XyzabC");
    }
}